# About
SendPulse test task

# Installation
- clone repository
### Docker installation
Run the following commands
- `composer install`
- `cp .env.example .env`
- `./vendor/bin/sail up -d`
- `docker-compose exec laravel.test php artisan key:generate`
- `docker-compose exec laravel.test php artisan migrate`

Application ready on "http://localhost/"

### WebServer installation
For deployment instructions please visit  [Laravel Deployment Documentation](https://laravel.com/docs/8.x/deployment)

Run the following commands
- `composer install`
- `cp .env.example .env`
- `php artisan key:generate`
- `php artisan migrate`

Application ready on "http://{domainName}"