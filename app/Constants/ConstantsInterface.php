<?php

namespace App\Constants;

interface ConstantsInterface
{
    const TAGS = [
        'link' => 'href',
        'img' => 'src',
        'script' => 'src',
        'a' => 'href',
    ];
}
