<?php

namespace App\Http\Controllers;

use App\Http\Requests\UrlRequest;
use App\Models\UrlCollection;
use App\Models\UrlModel;
use App\Services\HTML\HtmlServiceInterface;
use App\Services\URL\UrlServiceInterface;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;

class UrlController extends Controller
{
    /**
     * @var UrlServiceInterface
     */
    protected UrlServiceInterface $urlService;

    /**
     * @var HtmlServiceInterface
     */
    protected HtmlServiceInterface $htmlService;

    public function __construct(UrlServiceInterface $urlService, HtmlServiceInterface $htmlService)
    {
        $this->urlService = $urlService;
        $this->htmlService = $htmlService;
    }

    /**
     * Show start page
     *
     * @return View
     */
    public function index(): View
    {
        $data = UrlCollection::all();

        return view('index', ['data' => $data]);
    }

    /**
     * get data from requested URL
     *
     * @param UrlRequest $request
     *
     * @return RedirectResponse
     */
    public function getData(UrlRequest $request): RedirectResponse
    {
        $hash = $this->htmlService->store($request->sendPulseUrl);
        $this->urlService->store($request->sendPulseUrl, $hash);

        return redirect()->back()->with('success', 'Item created successfully!');
    }

    /**
     * Show stored pages
     *
     * @param string $hash
     *
     * @return string
     */
    public function show(string $hash): string
    {
        $url = UrlModel::where('hash', $hash)->first();

        return Storage::get($url->hash);
    }
}
