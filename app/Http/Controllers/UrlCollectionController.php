<?php

namespace App\Http\Controllers;

use App\Http\Requests\UrlCollectionRequest;
use App\Models\UrlCollection;
use App\Models\UrlModel;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UrlCollectionController extends Controller
{
    /**
     * Collection page
     *
     * @param string $slug
     *
     * @return View
     */
    public function index(string $slug): View
    {
        $data = UrlCollection::where('slug', $slug)->with('urlModel')->first();
        $urlData = UrlModel::whereNull('url_collection_id')->get();

        return view('collections.index', ['data' => $data, 'urlData' => $urlData->groupBy('domain')]);
    }

    /**
     * Store collection
     *
     * @param UrlCollectionRequest $request
     *
     * @return RedirectResponse
     */
    public function store(UrlCollectionRequest $request): RedirectResponse
    {
        UrlCollection::create([
                'name' => $request->sendPulseUrlCollection,
                'slug' => Str::of($request->sendPulseUrlCollection)->slug('-')
            ]);

        return redirect()->back()->with('success', 'Item created successfully!');
    }

    /**
     * Add URL to collection
     *
     * @param Request $request
     * @param string  $slug
     *
     * @return RedirectResponse
     */
    public function add(Request $request, string $slug): RedirectResponse
    {
        $collection = UrlCollection::where('slug', $slug)->first();

        $url = UrlModel::where('hash', $request->urlSelect)->first();
        $url->url_collection_id = $collection->id;
        $url->update();

        return redirect()->back();
    }
}
