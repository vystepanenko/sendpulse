<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UrlCollectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sendPulseUrlCollection' => 'required|unique:url_collections,name'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'sendPulseUrlCollection.required' => 'Collection name is required',
            'sendPulseUrlCollection.unique' => 'Collection name has already been taken',
        ];
    }
}
