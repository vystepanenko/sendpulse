<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UrlRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sendPulseUrl' => 'required|url|unique:url_models,address'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'sendPulseUrl.required' => 'URL address is required',
            'sendPulseUrl.url' => 'Input value must be a valid URL',
            'sendPulseUrl.unique' => 'URL address must be unique',
        ];
    }
}
