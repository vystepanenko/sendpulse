<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UrlCollection extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'slug'];

    public function urlModel()
    {
        return $this->hasMany(UrlModel::class);
    }
}
