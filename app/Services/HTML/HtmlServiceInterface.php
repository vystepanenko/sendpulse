<?php

namespace App\Services\HTML;

interface HtmlServiceInterface
{
    /**
     * store file from requested url
     *
     * @param string $incomingUrl
     *
     * @return string
     */
    public function store(string $incomingUrl): string;
}
