<?php

namespace App\Services\HTML;

use App\Constants\ConstantsInterface;
use DOMDocument;
use Illuminate\Support\Facades\Storage;

class HtmlService implements HtmlServiceInterface
{
    /**
     * store file from requested url
     * 
     * @param string $incomingUrl
     *
     * @return string
     */
    public function store(string $incomingUrl): string
    {
        $hash = hash('sha256', $incomingUrl);
        $domHtml = $this->getHtml($incomingUrl);
        $html = $this->parseHtml($domHtml, $incomingUrl);

        Storage::put($hash, $html);

        return $hash;
    }

    /**
    * parse HTML and replace relative paths to absolute
    *
    * @param DOMDocument $dom
    * @param string      $incomingUrl
    *
    * @return string
    */
    protected function parseHtml(DOMDocument $dom, string $incomingUrl): string
    {
        $scheme = $this->parseUrl($incomingUrl);

        foreach (ConstantsInterface::TAGS as $element => $attribute) {
            $elements = $dom->getElementsByTagName($element);

            foreach ($elements as $el) {
                if (preg_match('/http/i', $el->getAttribute($attribute))) {
                    continue;
                }

                $el->setAttribute($attribute, $scheme . $el->getAttribute($attribute));
            }
        }

        return $dom->saveHTML();
    }

    /**
     * get html from incoming URL
     *
     * @param string $incomingUrl
     *
     * @return DOMDocument
     */
    protected function getHtml(string $incomingUrl): DOMDocument
    {
        $dom = new DOMDocument;
        $dom->loadHTMLFile($incomingUrl, LIBXML_NOWARNING | LIBXML_NOERROR);

        return $dom;
    }

    /**
    * parse incoming URL string
    *
    * @param mixed $url
    *
    * @return string
    */
    protected function parseUrl($url): string
    {
        $parsedUrl = parse_url($url);
        $scheme = isset($parsedUrl['scheme']) ? $parsedUrl['scheme'] . '://' : '';
        $host = isset($parsedUrl['host']) ? $parsedUrl['host'] : '';
        $port = isset($parsedUrl['port']) ? ':' . $parsedUrl['port'] : '';

        return "$scheme$host$port";
    }
}
