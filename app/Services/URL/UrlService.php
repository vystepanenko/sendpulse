<?php

namespace App\Services\URL;

use App\Models\UrlModel;
use Illuminate\Support\Facades\Storage;

class UrlService implements UrlServiceInterface
{
    /**
     * store URL
     *
     * @param string $incomingUrl
     *
     * @return void
     */
    public function store(string $incomingUrl, string $hash): void
    {
        UrlModel::create([
            'hash' => $hash,
            'address' => $incomingUrl,
            'price' => $this->getPrice($hash),
            'domain' => $this->getDomain($incomingUrl)
        ]);
    }

    /**
     * get price for file
     *
     * @param string $hash
     *
     * @return float
     */
    protected function getPrice(string $hash): float
    {
        return round(Storage::size($hash) / 1024) * 0.001;
    }

    /**
     * get domain from incoming url
     *
     * @param string $incomingUrl
     *
     * @return string
     */
    protected function getDomain(string $incomingUrl): string
    {
        return parse_url($incomingUrl, PHP_URL_HOST);
    }
}
