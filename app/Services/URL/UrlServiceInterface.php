<?php

namespace App\Services\URL;

interface UrlServiceInterface
{
    /**
     * store URL
     *
     * @param string $incomingUrl
     * @param string $hash
     *
     * @return void
     */
    public function store(string $incomingUrl, string $hash): void;
}
