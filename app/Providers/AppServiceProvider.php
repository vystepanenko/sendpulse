<?php

namespace App\Providers;

use App\Services\HTML\HtmlService;
use App\Services\HTML\HtmlServiceInterface;
use App\Services\URL\UrlService;
use App\Services\URL\UrlServiceInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UrlServiceInterface::class, UrlService::class);
        $this->app->bind(HtmlServiceInterface::class, HtmlService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
