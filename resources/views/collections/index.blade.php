@extends('layouts.default')

@section('content')
<div class="container">
    <h1>Collection "{{ $data->name }}"</h1>
</div>

<div class="container mt-5">
    @if ($urlData->count() > 0)
    <form class="row "
          method="POST"
          action="/collection/add/{{ $data->slug }}">
        @csrf
        <div class="col">
            <label for="urlSelect"
                   class="form-label fs-4">Add URL to collection</label>
        </div>

        <div class="col-6">
            <select class="form-select"
                    id="urlSelect"
                    name="urlSelect">
                @foreach ($urlData as $key => $domain)
                <optgroup label="{{ $key }}">
                    @foreach ($domain as $item)
                    <option value="{{ $item->hash }}">{{ $item->address }}</option>
                    @endforeach
                </optgroup>
                @endforeach
            </select>
        </div>

        <div class="col">
            <button type="submit"
                    class="btn btn-primary mb-3">add</button>
        </div>
    </form>
    @else
    <div class="alert alert-warning alert-block">
        There is no URLs to add. Please go to start page and add one
    </div>
    @endif
</div>

<div class="container mt-5">
    <hr>
</div>

@if ($data->urlModel->count()>0)
<div class="container mt-5">
    <div class="row">
        <h3>URL list</h3>

        <table class="table mt-2">
            <thead>
                <tr>
                    <th scope="col">Addres</th>
                    <th scope="col">Price</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data->urlModel as $item)
                <tr>
                    <th scope="row"><a href="/{{ $item->hash }}">{{ $item->address }}</a></th>
                    <td>${{ $item->price }}</td>
                </tr>
                @endforeach
                <th>Total price:</th>
                <th>${{ $data->urlModel->sum('price') }}</th>
            </tbody>
        </table>

    </div>
</div>
@endif

@endsection