@extends('layouts.default')

@section('content')

@if ($errors->any())
<div class="container mt-5">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

@if ($message = Session::get('success'))
<div class="container mt-5">
    <div class="row">
        <div class="alert alert-success alert-block">
            <strong>{{ $message }}</strong>
        </div>
    </div>
</div>
@endif

<div class="container mt-5">
    <div class="row">

        <form action="getData"
              method="POST">
            @csrf
            <div class="row justify-content-around">

                <div class="col">
                    <label for="sendPulseUrl"
                           class="form-label fs-4">Enter URL</label>
                </div>

                <div class="col-6">
                    <input type="url"
                           class="form-control"
                           id="sendPulseUrl"
                           name="sendPulseUrl"
                           aria-describedby="urlHelp">
                </div>

                <div class="col">
                    <button type="submit"
                            class="btn btn-primary">Add</button>
                </div>
            </div>

        </form>
    </div>
</div>

<div class="container mt-5">
    <hr>
</div>

<div class="container mt-5">
    <div class="row justify-content-around">

        <form action="collection/store"
              method="POST">
            @csrf
            <div class="row justify-content-around">

                <div class="col">
                    <label for="sendPulseUrl"
                           class="form-label fs-4">Enter collection name</label>
                </div>

                <div class="col-6">
                    <input type="text"
                           class="form-control"
                           id="sendPulseCollection"
                           name="sendPulseUrlCollection"
                           aria-describedby="urlCollectionHelp">
                </div>

                <div class="col">
                    <button type="submit"
                            class="btn btn-primary">Add</button>
                </div>
            </div>
        </form>
    </div>
</div>

@if ($data->count() > 0)
<div class="container mt-5">
    <div class="row">
        <p class="fs-4">Collection list</p>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $item)
                <tr>
                    <th scope="row">{{ $loop->index+1 }}</th>
                    <td><a href="{{ 'collection/' . $item->slug }}">{{ $item->name }}</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
@endif

@endsection