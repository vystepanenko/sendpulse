<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUrlModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('url_models', function (Blueprint $table) {
            $table->id();
            $table->text('address');
            $table->text('domain');
            $table->text('hash');
            $table->unsignedInteger('url_collection_id')->nullable();
            $table->float('price', 8, 4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('url_models');
    }
}
