<?php

use App\Http\Controllers\UrlCollectionController;
use App\Http\Controllers\UrlController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UrlController::class, 'index']);
Route::post('/getData', [UrlController::class, 'getData']);
Route::get('/{hash}', [UrlController::class, 'show']);

Route::prefix('collection')->group(function () {
    Route::post('/store', [UrlCollectionController::class, 'store']);
    Route::post('/add/{slug}', [UrlCollectionController::class, 'add']);
    Route::get('/{slug}', [UrlCollectionController::class, 'index']);
});
